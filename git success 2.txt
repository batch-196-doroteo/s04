
Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04
$ git init
Initialized empty Git repository in C:/Users/Emie/Desktop/batch-196/s04/.git/

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git add .
starting fsmonitor-daemon in 'C:/Users/Emie/Desktop/batch-196/s04'

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git commit -m "added: s04 solution"
[master (root-commit) f0cfbc8] added: s04 solution
 9 files changed, 256 insertions(+)
 create mode 100644 activity/activity.html
 create mode 100644 activity/movie/top5ActorActresses.html
 create mode 100644 activity/movie/top5Movie.html
 create mode 100644 activity/music/top5Artists.html
 create mode 100644 activity/music/top5Music.html
 create mode 100644 discussion/images/haru.jpg
 create mode 100644 discussion/images/relative.html
 create mode 100644 discussion/index.html
 create mode 100644 discussion/link.html

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git remote add origin git@gitlab.com:batch-196-doroteo/s04.git

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git remote -v
origin  git@gitlab.com:batch-196-doroteo/s04.git (fetch)
origin  git@gitlab.com:batch-196-doroteo/s04.git (push)

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git push origin master
Enumerating objects: 16, done.
Counting objects: 100% (16/16), done.
Delta compression using up to 2 threads
Compressing objects: 100% (16/16), done.
Writing objects: 100% (16/16), 38.96 KiB | 1.26 MiB/s, done.
Total 16 (delta 3), reused 0 (delta 0), pack-reused 0
To gitlab.com:batch-196-doroteo/s04.git
 * [new branch]      master -> master

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git status
On branch master
nothing to commit, working tree clean

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        git.txt

nothing added to commit but untracked files present (use "git add" to track)

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git init
Reinitialized existing Git repository in C:/Users/Emie/Desktop/batch-196/s04/.gi
t/

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git add .

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git commit -m "add git text file"
[master b6b719b] add git text file
 1 file changed, 48 insertions(+)
 create mode 100644 git.txt

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git push origin master
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 2 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 915 bytes | 457.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.com:batch-196-doroteo/s04.git
   f0cfbc8..b6b719b  master -> master

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git log
commit b6b719b9b73b7584d78e969d0ac3bd1920b36b63 (HEAD -> master, origin/master)
Author: camidoroteo <camille.doroteo7@gmail.com>
Date:   Wed Jul 6 07:13:31 2022 +0800

    add git text file

commit f0cfbc82a8a74915aa1fd545bb6876dec7d4e85a
Author: camidoroteo <camille.doroteo7@gmail.com>
Date:   Wed Jul 6 07:07:50 2022 +0800

    added: s04 solution

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git status
On branch master
nothing to commit, working tree clean

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ ^C

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git status
On branch master
Untracked files:
  (use "git add <file>..." to include in what will be committed)
        git success.txt

nothing added to commit but untracked files present (use "git add" to track)

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git init
Reinitialized existing Git repository in C:/Users/Emie/Desktop/batch-196/s04/.gi
t/

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git add .

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git commit -m "add git success text file"
[master 95f6a00] add git success text file
 1 file changed, 99 insertions(+)
 create mode 100644 git success.txt

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$ git push origin master
Enumerating objects: 4, done.
Counting objects: 100% (4/4), done.
Delta compression using up to 2 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 1.28 KiB | 654.00 KiB/s, done.
Total 3 (delta 0), reused 0 (delta 0), pack-reused 0
To gitlab.com:batch-196-doroteo/s04.git
   b6b719b..95f6a00  master -> master

Emie@LAPTOP-LU5GDCAL MINGW64 ~/Desktop/batch-196/s04 (master)
$
